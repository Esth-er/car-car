from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    import_href = models.CharField(max_length=200, unique=True)

class Technician(models.Model):
    tech_name = models.CharField(max_length=100)
    emp_number = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.tech_name

class ServiceAppointment (models.Model):
    vin = models.CharField(max_length=20)
    owner = models.CharField(max_length=100)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    assigned_tech = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )
    reason = models.TextField()
    vip = models.BooleanField(default=False)
    cancel = models.BooleanField(default=False)
    finish = models.BooleanField(default=False)

    def __str__(self):
        return self.owner
