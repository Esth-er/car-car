import json
from common.json import ModelEncoder
from service_rest.models import Technician, ServiceAppointment, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "color",
        "year",
        "model",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "tech_name",
        "emp_number",
        "id",
    ]

class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "vin",
        "owner",
        "assigned_tech",
        "reason",
        "id",
    ]
    encoders = {
        "assigned_tech": TechnicianEncoder(),
    }
    def get_extra_data(self, o):
        date = json.dumps(o.date, default=str)
        time = json.dumps(o.time, default=str)
        date = json.loads(date)
        time = json.loads(time)
        return {
            "date": date,
            "time": time,
        }
