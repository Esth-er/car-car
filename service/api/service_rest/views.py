from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from service_rest.models import AutomobileVO, Technician, ServiceAppointment
from service_rest.encoders import AutomobileVOEncoder, TechnicianEncoder, ServiceAppointmentEncoder

# Create your views here.

@require_http_methods(["GET", "POST"])
def api_list_techs(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {"techs": techs},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            tech = Technician.objects.create(**content)
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Invalid technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_tech(request, id):
    if request.method == "GET":
        try:
            tech = Technician.objects.get(id=id)
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            tech = Technician.objects.get(id=id)
            tech.delete()
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            tech = Technician.objects.get(id=id)

            props = ["tech_name"]
            for prop in props:
                if prop in content:
                    setattr(tech, prop, content[prop])
            tech.save()
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_list_appts(request, vin=None):
    if request.method == "GET":
        if vin == None:
            appts= ServiceAppointment.objects.all()
        else:
            vin = vin
            appts = ServiceAppointment.objects.filter(vin=vin)
        return JsonResponse(
            {"appts": appts},
            encoder=ServiceAppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            tech = Technician.objects.get(id=content["assigned_tech"])
            content["assigned_tech"] = tech

        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=400,
            )
        appt = ServiceAppointment.objects.create(**content)
        return JsonResponse(
                appt,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )

@require_http_methods(["GET"])
def api_appt_history(request, vin):
    if request.method == "GET":
        print(request)
        try:
            appt = ServiceAppointment.objects.filter(vin=vin)
            return JsonResponse(
                appt,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400,
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appt(request, id):
    if request.method == "GET":
        try:
            appt = ServiceAppointment.objects.get(id=id)
            return JsonResponse(
                appt,
                encoder=ServiceAppointmentEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appt = ServiceAppointment.objects.get(id=id)
            appt.delete()
            return JsonResponse(
                appt,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            ServiceAppointment.objects.filter(id=id).update(**content)
            appt = ServiceAppointment.objects.get(id=id)
            return JsonResponse(
                appt,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
