from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.core.serializers import serialize
import json

from .encoders import (SaleListEncoder,
                       SaleDetailEncoder,
                       CustomerListEncoder,
                       CustomerDetailEncoder,
                       SalespersonEncoder)

from .models import (Sale,
                     Salesperson,
                     Customer,
                     AutomobileVO)

from .acls import get_car_photo


# ** List all sales and list sales by salesperson
@require_http_methods(["GET", "POST"])
def list_or_create_sales(request, ):
    if request.method == "GET":
        sales = Sale.objects.all()
        sales_serialized = serialize("json", sales)
        sales_serialized = json.loads(sales_serialized)
        print(sales_serialized)
        return JsonResponse(
            {"sales": sales_serialized},
            encoder=SaleListEncoder,
            safe=False,
            status=200,
        )

    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
            salesperson = Salesperson.objects.get(last_name=content["salesperson"])
            content["salesperson"] = salesperson
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
            sale_price = content["sale_price"]
        except:
            response = JsonResponse(
                {"message": "Could not create sale"}
            )
            response.status_code = 404
            return response

        sale = Sale.objects.create(**content)

        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False,
        )


# ** Show sale details
@require_http_methods(["GET", "DELETE", "PUT"])
def show_or_edit_sale(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        picture = get_car_photo(
            sale.manufacturer.model,
        )
        return JsonResponse(
            {"sale": sale, "photo": photo},
            encoder=SaleDetailEncoder,
        )
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleDetailEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist"}
            )
    else:
        try:
            content = json.loads(request.body)
            sale = Sale.objects.get(id=id)

            props = ["salesperson", "customer", "sale_price"]
            for prop in props:
                if prop in content:
                    setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_or_create_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def show_or_edit_customer(request, id):
    if request.method == "GET":
        customer = Customer.object.get(id=id)
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerDetailEncoder,
        )
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"}
            )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id)

            props = ["street_address", "city", "state"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()

        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response

        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
            )


@require_http_methods(["GET", "POST"])
def list_or_create_salesperson(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salesperson},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            # employee_number = content["employee_number"]
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create salesperson"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def show_or_edit_salesperson(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        try:
            pass
        except:
            pass
    else: # delete
        try:
            pass
        except:
            pass
