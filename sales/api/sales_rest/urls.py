from django.urls import path

from .views import (
    list_or_create_sales,
    show_or_edit_sale,
    list_or_create_customer,
    show_or_edit_customer,
    list_or_create_salesperson,
    show_or_edit_salesperson,
)

urlpatterns = [
    path("sales/", list_or_create_sales, name="list_or_create_sales"),
    path("sales/<int:id>/", show_or_edit_sale, name="show_or_edit_sale"),
    path("customers/", list_or_create_customer, name="list_or_create_customer"),
    path("customers/<int:id>", show_or_edit_customer, name="show_or_edit_customer"),
    path("salesperson/", list_or_create_salesperson, name="list_or_create_salesperson"),
    path("salesperson/<int:id>", show_or_edit_salesperson, name="show_or_edit_salesperson"),
]
