from django.contrib import admin

from .models import Sale, Customer, Salesperson, AutomobileVO


# Register your models here.
@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    pass



@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass


@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    pass

@admin.register(AutomobileVO)
class AutomobioleVOAdmin(admin.ModelAdmin):
    pass
