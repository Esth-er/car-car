import json
import requests
import os

UNSPLASH_API_KEY = "QRpvr1A1dI0QfS9ErhVVJGDYQNAOSSC2rAWLCZhsCsg"

def get_car_photo(manufacturer):
    headers = {"Authorization": UNSPLASH_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{manufacturer} car",
    }
    url = "https://api.unsplash.com/"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
