import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import SaleList from './list_view_pages/SaleList';
import SaleRecordForm from './forms/SaleRecordForm';
import SalespersonList from './list_view_pages/SalespersonList';
import SalespersonForm from './forms/SalespersonForm';
import CustomerList from './list_view_pages/CustomerList';
import CustomerForm from './forms/CustomerForm';

import ManufacturersList from './list_view_pages/ManufacturersList';
import ManufacturerForm from './forms/ManufacturerForm';
import VehicleModelsList from './list_view_pages/VehicleModelsList';
import VehicleModelForm from './forms/VehicleModelForm';
import AutomobilesList from './list_view_pages/AutomobilesList';
import AutomobileForm from './forms/AutomobileForm'

import TechnicianForm from './forms/TechnicianForm';
import ServiceAppointmentsList from './list_view_pages/ServiceAppointmentList';
import ServiceAppointmentForm from './forms/ServiceAppointmentForm';
import ServiceHistory from './list_view_pages/ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="sales">
            <Route path="sales-list" element={<SaleList />} />
            <Route path="create" element={<SaleRecordForm />} />
            <Route path="salesperson-list" element={<SalespersonList />} />
            <Route path="create-salesperson" element={<SalespersonForm />} />
            <Route path="customers-list" element={<CustomerList />} />
            <Route path="create-customer" element={<CustomerForm />} />
          </Route>

          <Route path="technicians">
            <Route path="create" element={<TechnicianForm />} />
          </Route>

          <Route path="service">
            <Route path="list" element={<ServiceAppointmentsList />} />
            <Route path="create" element={<ServiceAppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>

          <Route path="manufacturers">
            <Route path="list" element={<ManufacturersList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>

          <Route path="models">
            <Route path="list" element={<VehicleModelsList />} />
            <Route path="create" element={<VehicleModelForm />} />
          </Route>

          <Route path="automobiles">
            <Route path="list" element={<AutomobilesList />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
