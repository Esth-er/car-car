import { useState, useEffect } from 'react';


function AutomobilesList() {
    const [automobiles, setAutomobiles] = useState([])
    const getData = async() => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        const data = await response.json()

        setAutomobiles(data.autos)
    }
    useEffect(()=> {
        getData();
    }, []
    )

    return (
        <div>
            <h1>Automobiles</h1>
            <table className="table table-dark table-hover table-striped">
                <thead>
                    <tr>
                        <th>Color</th>
                        <th>Year</th>
                        <th>VIN</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles?.map(automobile => {
                        return(
                            <tr key={automobile.vin}>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.vin}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.model.manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default AutomobilesList;
