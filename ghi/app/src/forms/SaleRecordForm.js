import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';


const initialState = {
        automobile: "",
        salesperson: "",
        customer: "",
        sale_price: "",
    };

  const SaleForm = () => {
    let navigate = useNavigate();

    const [ formData, setFormData ] = useState(initialState)
    const [ automobiles, setAutomobiles ] = useState([])
    const [ salespeople, setSalespeople ] = useState([])
    const [ customers, setCustomers ] = useState([])


    useEffect(() => {
      const loadData = async () => {
        const customersUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customersUrl);

        if (response.ok) {
          const data = await response.json();
          setCustomers(data.customers);
        } else {
          console.log("There was an error loading the customers")
        }
      }

      loadData()

    }, [])

    useEffect(() => {
      const loadData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salesperson/';
        const response = await fetch(salespeopleUrl);

        if (response.ok) {
          const data = await response.json();
          setSalespeople(data.salespeople);
        } else {
          console.log("There was an error loading these salespeople")
        }
      }

      loadData()

    }, [])


  useEffect(() => {
    const loadData = async () => {
      const automobileUrl = 'http://localhost:8100/api/automobiles/';
      const response = await fetch(automobileUrl);

      if (response.ok) {
        const data = await response.json();
        console.log(data)
        setAutomobiles(data.autos);
      } else {
        console.log("There was an error loading these automobiles")
      }
    }

    loadData()

  }, [])

  const handleFormChange = (e) => {
      setFormData({
          ...formData,
          [e.target.name]: e.target.value
      })
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    const salesUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {

        setFormData(initialState);
        navigate("/sales/sales-list")
    }
  }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">

                    <div className="mb-3">
                      <select onChange={handleFormChange} value={formData.automobile} className="form-select" required name="automobile" id="vin">

                        <option value="">Choose an automobile</option>
                        {automobiles?.map(automobile => {
                          return (
                            <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                          )
                        })}
                      </select>
                    </div>

                    <div className="mb-3">
                      <select onChange={handleFormChange} value={formData.salesperson} className="form-select" required name="salesperson" id="salesperson">

                        <option value="">Choose a salesperson</option>
                        {salespeople?.map(salesperson => {
                          return (
                            <option key={salesperson.id} value={salesperson.last_name}>{salesperson.last_name}</option>
                          )
                        })}
                      </select>
                    </div>

                    <div className="mb-3">
                      <select onChange={handleFormChange} value={formData.customer} className="form-select" name="customer" id="customer" required>

                        <option value="">Choose a customer</option>
                        {customers?.map(customer => {
                          return (
                            <option key={customer.id} value={customer.id}>{customer.last_name}</option>
                          )
                        })}
                      </select>
                    </div>

                    <div className="form-floating mb-3">
                        <input value={formData.sale_price} onChange={handleFormChange} placeholder="Sale price" required type="number" name="sale_price" id="sale_price" className="form-control" />
                        <label htmlFor="sale_price">Sale price</label>
                    </div>

                    <button className="btn btn-primary">Record sale</button>

                </form>
            </div>
        </div>
    </div>
    );
}

export default SaleForm;
