import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const initialState = {
        first_name: "",
        last_name: "",
        employee_number: "",
    };

    const SalespersonForm = () => {
        let navigate = useNavigate();

        const [ formData, setFormData ] = useState(initialState);

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const salespersonUrl = 'http://localhost:8090/api/salesperson/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {

            setFormData(initialState);
            navigate('/sales/salesperson-list/')
        }
    }

    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new salesperson</h1>
                <form onSubmit={handleSubmit} id="create-salesperson-form">

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.first_name} placeholder="First Name" required type="text" name="first_name" id="First name" className="form-control" />
                        <label htmlFor="first_name">First name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" required type="text" name="last_name" id="Last name" className="form-control" />
                        <label htmlFor="last_name">Last name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.employee_number} placeholder="Employee Number" required type="text" name="employee_number" id="Employee number" className="form-control" />
                        <label htmlFor="Employee_number">Employee number</label>
                    </div>

                    <button className="btn btn-primary">Create new salesperson</button>

                </form>
            </div>
        </div>
    </div>
    );
}

export default SalespersonForm;
